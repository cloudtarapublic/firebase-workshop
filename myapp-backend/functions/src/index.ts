import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
const cors = require('cors')({ origin: true });

admin.initializeApp(
  functions.config().firebase
);

const db = admin.database();

export const saveUser = functions.https.onRequest((request, response) => {
  cors(request, response, async () => {
    if (request.method === 'POST') {
      const data = {
        name: request.body['name'],
        email: request.body['email']
      }

      await db.ref('/registeration').push(data);

      response.send({
        msg: 'Saved App Statistics Successfully'
      });
    }
  });
});


export const helloWorld = functions.https.onCall(async (data: any, context: functions.https.CallableContext) => {
  console.log("I am helloWorld:", JSON.stringify(data));

  await db.ref('/registeration').push(data);

  return "success from function";
});
