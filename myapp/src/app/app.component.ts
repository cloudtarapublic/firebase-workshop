import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AngularFireFunctions } from '@angular/fire/functions';
import { HttpClient } from '@angular/common/http';
import { AngularFireDatabase } from '@angular/fire/database';
import { take } from 'rxjs/operators';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private functions: AngularFireFunctions,
    private http: HttpClient,
    private db: AngularFireDatabase
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  ngOnInit() {
    const callable = this.functions.httpsCallable('helloWorld');
    callable({ name: 'name' }).subscribe((res) => {
      console.log("success", res)
    },
    (error) => {
      console.log("error", error)
    });

    this.http.post('http://localhost:5000/fosscon-workshop/us-central1/' + 'saveUser', {
      name: 'name',
      email: 'email'
    }).subscribe((res) => {
      console.log("success", res)
    },
    (error) => {
      console.log("error", error)
    });

    this.db.object('/registeration').valueChanges().pipe().subscribe((res) => {
      console.log("success", res)
    },
    (error) => {
      console.log("error", error)
    });
    
  }
}
